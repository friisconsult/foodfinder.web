﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FoodFinder.Web.Authentication;
using FoodFinder.Web.Model;
using Newtonsoft.Json;

namespace FoodFinder.Web.Controllers
{
    [Route("Venue")]
    public class VenueController : Controller
    {
        private readonly TemplateContext _db;
        public VenueController(TemplateContext db)
        {
            _db = db;
        }

        public async Task<IActionResult> Index()
        {
            var Auth0Client = new HttpClient();
            Auth0Client.DefaultReq
            
            "https://friisconsult.eu.auth0.com/oauth/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"client_id\":\"lfqJT2qgFnDeArKxH1fGUSkZJgOCnmnm\",\"client_secret\":\"E68Zp1eggm7uHyVkPyqmnu4wMfDuhHv-EMtG1kbP9NfYtGWIkNMSxMX7gdydNrEz\",\"audience\":\"https://foodfinderapi.azurewebsites.net\",\"grant_type\":\"client_credentials\"}", ParameterType.RequestBody);
            IRestResponse tokenresponse = client.Execute(request);
            
            
            
            var client = new RestClient("https://foodfinderapi.azurewebsites.net/api/v1/venues/");
            var request = new RestRequest(Method.GET);
            request.AddHeader("authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5VRkROamRGTXpBME9ERXdOVGxGT0RJd09FWkdRVGsxTlRJMk9EYzJOelV6UWtORk1qVTVRdyJ9.eyJpc3MiOiJodHRwczovL2ZyaWlzY29uc3VsdC5ldS5hdXRoMC5jb20vIiwic3ViIjoibGZxSlQycWdGbkRlQXJLeEgxZkdVU2taSmdPQ25tbm1AY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vZm9vZGZpbmRlcmFwaS5henVyZXdlYnNpdGVzLm5ldCIsImlhdCI6MTUyMjE0ODQ0NSwiZXhwIjoxNTIyMjM0ODQ1LCJhenAiOiJsZnFKVDJxZ0ZuRGVBckt4SDFmR1VTa1pKZ09Dbm1ubSIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.k7KpM2HS-STQavEgLytENfaCbekMrcuhcHsTndvmkYetU--_Ou6LRzV3NsOlSbgZVMa-4HTgd4UFJjR-dX4bcOeBBUMMc-G1hHqTYEAQDIsTFtZI3Db_hHfVwcJ1WLZpzxRkf_5J_EHx7x73wWUhePyyc0T9ywqHx1GD8ZgtzD2D3PkqNRHOMXEc3_Waum7Q8NbI_MLaG6tpnRuFx46o4CxohV0RSIjVf81kSc5E5JHQX5k2cXngGw7srcW_v4O3xnyZf0MTDoO548CDLKs2KsSTVulIWLd8LiRmvcgmiuujMmlWkDgxYKKEvGExXcF8tuiAiowehpxMHPBbtG4Mpg");
           
            
            
            using (var foodFinderApi = new HttpClient())
            {
                var response = await foodFinderApi.GetAsync("https://foodfinderapi.azurewebsites.net/api/v1/venues");

                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    var venues = JsonConvert.DeserializeObject<Venue[]>(jsonString);

                    return View(venues);
                }
            }

            return View(_db.Venues.Take(10).ToList());
        }

        [Route("{id}")]
        public async Task<IActionResult> Venue(Guid id)
        {
            using (var foodFinderApi = new HttpClient())
            {
                var response = await foodFinderApi.GetAsync($"https://foodfinderapi.azurewebsites.net/api/v1/venues{id.ToString()}");

                if (!response.IsSuccessStatusCode) return NotFound();

                var jsonString = await response.Content.ReadAsStringAsync();
                var venue = JsonConvert.DeserializeObject<Venue>(jsonString);

                var menuResponse =
                    await foodFinderApi.GetAsync($"https://foodfinderapi.azurewebsites.net/api/v1/menuitem/menu/{venue.Id.ToString()}");

                if (menuResponse.IsSuccessStatusCode)
                {
                    var memuItemsJson = await menuResponse.Content.ReadAsStringAsync();
                    venue.MenuItems = JsonConvert.DeserializeObject<MenuItem[]>(memuItemsJson);

                }

                var reviewresponse = await foodFinderApi.GetAsync(
                    $"https://foodfinderapi.azurewebsites.net/api/v1/reviews/venue/{venue.Id.ToString()}");

                if (!reviewresponse.IsSuccessStatusCode) return View(venue);

                var reviewsJson = await reviewresponse.Content.ReadAsStringAsync();
                venue.Reviews = JsonConvert.DeserializeObject<Review[]>(reviewsJson);

                return View(venue);
            }
        }

        [HttpGet, Route("create")]
        public IActionResult Create()
        {
            Contract.Ensures(Contract.Result<IActionResult>() != null);

            //ViewBag.FoodType = items;
            return View();
        }

        [HttpPost, Route("create")]
        public async Task<IActionResult> CreateVenue(Venue venue)
        {
            using (var foodFinderApi = new HttpClient())
            {
                venue.Owner = User.GetUserId();
                var venues = new Venue[] { venue };


                var jsonString = JsonConvert.SerializeObject(venues);
                var stringContext = new StringContent(jsonString, Encoding.UTF8, "application/json");


                foodFinderApi.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                var response = await foodFinderApi.PostAsync("https://foodfinderapi.azurewebsites.net/api/v1/venues", stringContext);

                if (response.IsSuccessStatusCode)
                    return RedirectToAction("Index");


            }

            return Ok();
        }
    }
}
