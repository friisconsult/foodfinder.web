﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FoodFinder.Web.Model;

namespace FoodFinder.Web.Controllers
{
	public class HomeController : Controller
	{
		public IActionResult Index()
		{
			return View();
		}
	

		public IActionResult Error()
		{
			return View();
		}
	}
}
